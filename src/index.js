const express = require('express');
const exphbs = require('express-handlebars');
const { extname } = require('path');
const path = require('path');
//Inicializamos la App
const app = express();
//Inicializamos las configuraciones
app.set('port',process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.use(require('./routes/indexController'));
app.use(require('./routes/homeController'));
app.use(require('./routes/signinController'));
app.use(require('./routes/logoutController'));
app.use(express.static(path.join(__dirname, 'public')));

app.engine('.hbs', exphbs({
    defaultLayout:'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname:'.hbs',
}));
app.set('view engine', 'hbs');
//Corremos el Servidor
app.listen( app.get('port'),()=>{
    console.log('El servidor esta corriendo en el puerto', app.get('port'));
} 
);